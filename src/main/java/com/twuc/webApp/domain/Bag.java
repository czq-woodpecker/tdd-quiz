package com.twuc.webApp.domain;

public class Bag {
    private BagSize bagSize;

    public Bag() {
        this.bagSize = bagSize.SMALL;
    }

    public Bag(BagSize bagSize) {
        this.bagSize = bagSize;
    }

    public BagSize getBagSize() {
        return bagSize;
    }
}
