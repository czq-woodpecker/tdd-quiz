package com.twuc.webApp.domain;

public class Ticket {
    private SlotSize slotSize;

    public Ticket(SlotSize slotSize) {
        this.slotSize = slotSize;
    }

    public Ticket() {
        this.slotSize = SlotSize.SMALL;
    }

    public SlotSize getSlotSize() {
        return slotSize;
    }
}
