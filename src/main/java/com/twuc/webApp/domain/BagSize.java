package com.twuc.webApp.domain;

public enum BagSize {
    SMALL(0), MEDIUM(1), BIG(2);
    private int size;

    BagSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
