package com.twuc.webApp.domain;


import java.util.HashMap;
import java.util.Map;

public class Storage {
    private Map<Ticket, Bag> bags = new HashMap<>();
    private int smallCapacity;
    private int mediumCapacity;
    private int bigCapacity;
    private int smallSlotCount;
    private int mediumSlotCount;
    private int bigSlotCount;

    public Storage(int smallCapacity) {
        this.smallCapacity = smallCapacity;
    }

    public Storage(int bigCapacity, int mediumCapacity, int smallCapacity) {
        if (bigCapacity >= 0 && mediumCapacity >= 0 && smallCapacity >= 0) {
            this.bigCapacity = bigCapacity;
            this.mediumCapacity = mediumCapacity;
            this.smallCapacity = smallCapacity;
            return;
        }
        throw new IllegalArgumentException("capacity argument must be equals or more than 0");
    }

    public Ticket save(Bag bag) {
        return this.save(bag, SlotSize.SMALL);
    }

    public Ticket save(Bag bag, SlotSize slotSizeType) {
        if (bag != null && !slotSizeType.canSave(bag.getBagSize())) {
            throw new IllegalStateException(
                    String.format("Cannot save your bag: %s %s",
                            bag.getBagSize().name(),
                            slotSizeType.name()));
        }
        switch (slotSizeType) {
            case BIG: {
                if (bigSlotCount < bigCapacity) {
                    bigSlotCount++;
                    Ticket ticket = new Ticket(slotSizeType);
                    bags.put(ticket, bag);
                    return ticket;
                }
                break;
            }
            case MEDIUM: {
                if (mediumSlotCount < mediumCapacity) {
                    mediumSlotCount++;
                    Ticket ticket = new Ticket(slotSizeType);
                    bags.put(ticket, bag);
                    return ticket;
                }
                break;
            }
            case SMALL: {
                if (smallSlotCount < smallCapacity) {
                    smallSlotCount++;
                    Ticket ticket = new Ticket(slotSizeType);
                    bags.put(ticket, bag);
                    return ticket;
                }
                break;
            }
        }
        throw new IllegalStateException("insufficient capacity");

    }

    public Bag retrieve(Ticket ticket) {
        if (bags.containsKey(ticket)) {
            switch (ticket.getSlotSize()) {
                case SMALL:
                    smallSlotCount--;
                    break;
                case MEDIUM:
                    mediumSlotCount--;
                    break;
                case BIG:
                    bigSlotCount--;
                    break;
            }
            return bags.remove(ticket);
        }
        throw new IllegalArgumentException("invalid ticket");
    }
}
