package com.twuc.webApp.domain;

public enum SlotSize {
    SMALL(0), MEDIUM(1), BIG(2);
    private int size;

    SlotSize(int size) {
        this.size = size;
    }

    boolean canSave(BagSize bagSize){
        return this.size >= bagSize.getSize();
    }

    public int getSize() {
        return size;
    }
}
