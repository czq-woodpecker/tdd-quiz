package com.twuc.webApp;

import com.twuc.webApp.domain.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ApplicationTest {

    public static Storage createStorageWithPlentyCapacity() {
        return new Storage(20);
    }

    @Test
    void should_get_a_ticket_when_saving_a_bag() {
        Storage storage = createStorageWithPlentyCapacity();
        Bag bag = new Bag();

        Ticket ticket = storage.save(bag);

        assertNotNull(ticket);
    }

    @Test
    void should_get_a_ticket_when_saving_nothing() {
        Storage storage = createStorageWithPlentyCapacity();
        final Bag nothing = null;

        Ticket ticket = storage.save(nothing);

        assertNotNull(ticket);
    }

    @Test
    void should_get_the_saved_bag_when_retrieving_the_bag_using_the_ticket() throws Exception {
        Storage storage = createStorageWithPlentyCapacity();
        Bag expectedBag = new Bag();
        Bag anotherBag = new Bag();
        Ticket ticket = storage.save(expectedBag);
        storage.save(anotherBag);

        Bag actualBag = storage.retrieve(ticket);

        assertSame(expectedBag, actualBag);
        assertNotSame(anotherBag, actualBag);
    }

    @Test
    void should_get_error_message_invalid_ticket_when_retrieving_the_bag_by_the_ticket_not_the_storage_created() {
        Ticket invalidTicket = new Ticket();
        Storage emptyStorage = createStorageWithPlentyCapacity();

        String expectedMessage = "invalid ticket";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> emptyStorage.retrieve(invalidTicket));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void should_get_error_message_invalid_ticket_when_retrieving_the_bag_by_the_used_ticket() {
        Storage storage = createStorageWithPlentyCapacity();
        Ticket usedTicket = storage.save(new Bag());
        storage.retrieve(usedTicket);

        String expectedMessage = "invalid ticket";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> storage.retrieve(usedTicket));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void should_get_error_message_invalid_ticket_when_retrieving_the_bag_by_the_null_ticket() {
        Storage storage = createStorageWithPlentyCapacity();
        storage.save(new Bag());
        Ticket nullTicket = null;

        String expectedMessage = "invalid ticket";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> storage.retrieve(nullTicket));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void should_get_nothing_when_save_nothing() {
        Storage storage = createStorageWithPlentyCapacity();
        Bag nothing = null;
        Ticket validTicket = storage.save(nothing);

        Bag actualGotThing = storage.retrieve(validTicket);

        assertNull(actualGotThing);
    }

    @Test
    void should_get_a_ticket_when_saving_the_bag_to_the_not_full_storage() {
        Storage notFullStorage = new Storage(2);
        Bag bag = new Bag();

        Ticket ticket = notFullStorage.save(bag);

        assertNotNull(ticket);
    }

    @Test
    void should_get_a_message_insufficient_capacity_when_saving_a_bag_to_a_full_storage() {
        Storage fullStorage = new Storage(1);
        Bag bag = new Bag();
        fullStorage.save(new Bag());

        String expectedMessage = "insufficient capacity";
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> fullStorage.save(bag));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void should_return_a_ticket_of_first_bag_and_error_message_insufficient_capacity_of_second_bag_when_saving_two_bags_to_a_empty_storage_which_capacity_is_one() {
        Storage storage = new Storage(1);
        Bag firstBag = new Bag();
        Bag secondBag = new Bag();

        Ticket ticket = storage.save(firstBag);
        assertNotNull(ticket);
        String expectedMessage = "insufficient capacity";
        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> storage.save(secondBag));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void should_able_to_save_another_bag_when_retrieving_a_bag_from_a_full_storage() {
        Storage storage = new Storage(1);
        Ticket ticket = storage.save(new Bag());

        storage.retrieve(ticket);
        assertDoesNotThrow(() -> storage.save(new Bag()));
    }

    @Test
    void should_able_to_save_bag_to_large_slot() {
        int bigSlotCount = 1;
        int mediumSlotCount = 0;
        int smallSlotCount = 0;
        Storage storage = new Storage(bigSlotCount, mediumSlotCount, smallSlotCount);
        Bag smallBag = new Bag(BagSize.SMALL);

        Ticket ticket = storage.save(smallBag, SlotSize.BIG);

        assertNotNull(ticket);
    }

    @Test
    void should_return_a_error_message_when_saving_the_big_bag_to_small_slot() {
        int bigSlotCount = 2;
        int mediumSlotCount = 2;
        int smallSlotCount = 2;
        Bag bigBag = new Bag(BagSize.BIG);
        Storage storage = new Storage(bigSlotCount, mediumSlotCount, smallSlotCount);

        IllegalStateException expection = assertThrows(IllegalStateException.class, () -> storage.save(bigBag, SlotSize.SMALL));
        assertEquals("Cannot save your bag: BIG SMALL", expection.getMessage());
    }

    @Test
    void should_get_a_valid_ticket_when_saving_the_bag_to_match_slot() {
        int bigSlotCount = 0;
        int mediumSlotCount = 1;
        int smallSlotCount = 0;
        Storage storage = new Storage(bigSlotCount, mediumSlotCount, smallSlotCount);
        Bag mediumBag = new Bag(BagSize.MEDIUM);

        Ticket ticket = storage.save(mediumBag, SlotSize.MEDIUM);

        assertNotNull(ticket);
        assertSame(mediumBag, storage.retrieve(ticket));
    }

    @Test
    void should_get_error_message_when_saving_a_bay_but_no_correct_slot() {
        int bigSlotSize = 0;
        int mediumSlotSize = 1;
        int smallSlotSize = 1;
        Storage storage = new Storage(bigSlotSize, mediumSlotSize, smallSlotSize);
        Bag mediumBag = new Bag(BagSize.MEDIUM);

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> storage.save(mediumBag, SlotSize.BIG));
        String expectedMessage = "insufficient capacity";
        assertEquals(expectedMessage, exception.getMessage());

    }
}